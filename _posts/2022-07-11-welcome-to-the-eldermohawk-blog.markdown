---
layout: default
title:  "Welcome to the Eldermohawk Blog"
date:   2022-07-11 10:32:14 -0300
categories: jekyll update
---
Welcome to the site! I'm pretty bad at updating these things so hopefully there's some new content coming soon.

Follow me on all the socials as I'm more likely to put stuff there: [Linktree](https://linktr.ee/ElderMohawk)
:)

A note on Jekyll for reference:

Jekyll offers powerful support for code snippets:

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

Check out the [Jekyll docs][jekyll-docs] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll’s GitHub repo][jekyll-gh]. If you have questions, you can ask them on [Jekyll Talk][jekyll-talk].

[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[jekyll-talk]: https://talk.jekyllrb.com/
