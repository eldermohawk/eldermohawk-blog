---
layout: default
title:  "Jekyll and Theme Issues"
date:   2022-07-11 15:18:14 -0300
categories: jekyll update
---
What a pain in the ass! I managed to get the repository started in Gitlab using the Pages template. Then, I personalized a bunch of stuff in the default config files. I've spent the rest of the day just fucking around with trying to implement a theme.

If you want to just slap together a Pages site with the default layout, no problem! But if you want to use a layout, even a default, built-in Pages layout, it takes so much more fiddly business. I have mine in place but it's still messed up in some places and I need to figure out implementing a home link in the top corner. Currently, only the about page is there and I'm writing this in hopes that it shows up in the header, too.

This is the most writing I've done in months but hopefully I can keep this up.
