---
layout: default
title: About
permalink: /about/
---
Just a blog by a guy who has an array of tech, coding, and crafting hobbies.
If you want to know more or would like to follow me on my various social media accounts, the linktree is below.

This is a Gitlab Pages site that uses Jekyll and the Jekyll Shell Theme

For info on how to use and customize Jekyll:
[jekyllrb.com](http://jekyllrb.com/)

You can find the source code for the Jekyll new theme at:
[jekyll-new](https://github.com/jglovier/jekyll-new)

You can find the source code for Jekyll at
[jekyll](https://github.com/jekyll/jekyll)
